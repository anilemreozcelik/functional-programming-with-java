package anonymousclass;

interface MyInterface{
    public void test();
}

abstract class AbstractClass{
    public abstract void test2();
}

public class AnonymousTest {

    public static void main(String[] args) {

        //Burada interface tipinde bir obje olusturmuyoruz!
        // MyInterface arabirimini uygulayan(implements) anonymous sinif tipinde bir obje olusturuyoruz.
        MyInterface myInterface=new MyInterface() {
            @Override
            public void test() {
                System.out.println("MyInterface");
            }
        };

        myInterface.test();


        AbstractClass abstractClass=new AbstractClass() {
            @Override
            public void test2() {
                System.out.println("AbstractClass");
            }
        };

        abstractClass.test2();
    }

}
