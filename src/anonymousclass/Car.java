package anonymousclass;

class Engine{
    public void speedUp(){
        System.out.println("Engine speedUp");
    }
}


public class Car {

    Engine engine=new Engine(){
        @Override
        public void speedUp() {
            System.out.println("Anonymous speedUp");
        }

        public void drift(){

        }
    };

    public static void main(String[] args) {
        Car car=new Car();
        car.engine.speedUp();

        //car.engine.drift();
    }
}
