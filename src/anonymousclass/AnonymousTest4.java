package anonymousclass;

class Anonymous3{
    void test(){

    }
}

public class AnonymousTest4 {

    private String shadowingExample="test";
    Anonymous3 anonymous3=new Anonymous3(){
        @Override
        void test() {
            String shadowingExample="test2";
            System.out.println(shadowingExample);
            System.out.println(AnonymousTest4.this.shadowingExample);
        }
    };

    public static void main(String[] args) {
        AnonymousTest4 anonymousTest4=new AnonymousTest4();
        anonymousTest4.anonymous3.test();
    }

}
