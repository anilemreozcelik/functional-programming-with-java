package anonymousclass;

class Anonymous2{
    void test(){

    }
}

public class AnonymousTest3 {

    private void outerMethod() {
        String localVariable="I am local variable";  //effectively final
        final String finalLocalVariable="I am final local variable"; //final
        Anonymous anonymous = new Anonymous() {
            void test() {
                System.out.println(localVariable);
                System.out.println(finalLocalVariable);
            }
        };

        anonymous.test();
    }

    public static void main(String[] args) {
        AnonymousTest3 anonymousTest3=new AnonymousTest3();
        anonymousTest3.outerMethod();
    }

}
