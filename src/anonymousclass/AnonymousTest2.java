package anonymousclass;

class Anonymous{
    void test(){

    }
}

public class AnonymousTest2 {
    private String var="private variable";

    private void outerMethod(){
        System.out.println("Outer method");
    }

    //Bir anonymous class , outer class in uyelerine erisebilir.(methods & variable)
    Anonymous anonymous=new Anonymous(){
        @Override
        void test() {
            System.out.println(var);
            outerMethod();
        }
    };

    public static void main(String[] args) {
        AnonymousTest2 anonymousTest2=new AnonymousTest2();
        anonymousTest2.anonymous.test();
    }
}
