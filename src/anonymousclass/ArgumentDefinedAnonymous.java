package anonymousclass;

interface MyInterface2{
    void test();
}

class MyClass{
    void doStuff(MyInterface2 myInterface){
        myInterface.test();
        System.out.println("MyClass doStuff");
    }
}

public class ArgumentDefinedAnonymous {

    public static void main(String[] args) {

        MyClass  myClass=new MyClass();
        myClass.doStuff(new MyInterface2() {
            @Override
            public void test() {
                System.out.println("Test Method");
            }
        });
    }
}
