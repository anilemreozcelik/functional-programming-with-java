package anonymousclass;

interface Math{
    int calculate(int arg1,int arg2);
}

public class AnonymousTest5 {

    public int doMath(Math math,int arg1,int arg2){
        return math.calculate(arg1,arg2);
    }

    public static void main(String[] args) {
        AnonymousTest5 anonymousTest5=new AnonymousTest5();
        Math m=new Math() {
            @Override
            public int calculate(int arg1, int arg2) {
                return arg1+arg2;
            }
        };
        System.out.println(anonymousTest5.doMath(m,5,3));

    }
}
