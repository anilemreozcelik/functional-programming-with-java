package _05.primitiveversions;

import behaviorparameterization.Predicate;

import java.util.function.DoublePredicate;
import java.util.function.IntPredicate;
import java.util.function.LongPredicate;
import java.util.stream.IntStream;

public class IntStreamTest01 {
    public static void main(String[] args) {

        //normal predicate
        Predicate<Integer> p1=(Integer i)->i%2==0;

        //bunda class vermiyoruz.Primitive versiyonunu kullanıyoruz.
        IntPredicate ip1=(int i)->i%2==0; //performans açısından daha iyi
        //IntPredicate
        //DoublePredicate
        //LongPredicate bunlardan başka yok primitive için.
        IntStream.range(1,10).filter(ip1).forEach(System.out::println);
    }
}
