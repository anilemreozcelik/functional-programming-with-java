package _02.consumer;

import java.util.function.Consumer;
import java.util.stream.Stream;

public class ConsumerTest01 {

    public static void main(String[] args) {
        Consumer<String> printUpperCase=str-> System.out.println(str.toUpperCase());
        printUpperCase.accept("hello");

        Stream.of("hello","world","message","text").forEach(printUpperCase);
    }
}
