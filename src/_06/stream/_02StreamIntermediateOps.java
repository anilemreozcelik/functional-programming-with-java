package _06.stream;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class _02StreamIntermediateOps {
    public static void main(String[] args) {

        Stream.of(11,22,33,44,55).peek(i-> System.out.print(i+" ")).count();
        //peek burada intermediate bir op.

        Stream<Integer> s1=Stream.of(11,22,33,44,55).peek(System.out::println);
        //intermediate operationlar lazy loading yapısındadır.
        //Bir terminal operation yoksa çalışmazlar.yukarıda da terminal op olmadığı için çıktı basmaz.


        //count metodu bir terminal operationdur.
        System.out.println(s1.count());

        Stream.of("one", "two", "three", "four")
                .filter(e->e.length()>3)
                .peek(e->System.out.println("Filtered Value: "+e))
                .map(String::toUpperCase)
                .peek(e->System.out.println("Mapped Value: "+e))
                .collect(Collectors.toList()).forEach(System.out::println);

        List<Integer> list
                = Arrays.asList(0, 2, 4, 6, 8, 10);

        // Using peek with count() method,Method
        // which is a terminal operation
        list.stream().peek(System.out::println).count();

        /*
        !!!
        Since Java 9, if JDK compiler is able computing the count directly from the stream (optimization in Java 9),
        it didn’t traverse the stream, so there is no need to run peek() at all.
        !!!
         */
        //IllegalStateException:stream has already been operated hatası verir.
        //birkez terminal op çağırdığımızda bir daha terminal op çağıramayız o stream üstünde
        //s1.forEach(System.out::println);


        //filter
        Stream<String> s2=Stream.of("ahmet","ali","mehmet","ayse","murat");
        s2.filter(str->str.startsWith("m")).forEach(System.out::println);

        IntStream.rangeClosed(0,10).filter(i->i%2==0).forEach(x->System.out.print(x+" "));

        System.out.println();

        //distinct
        Stream<String> s3=Stream.of("ahmet","ahmet","mehmet","ayşe","ayşe");
        s3.distinct().forEach(System.out::println);

        //map
        Stream<String> s4=Stream.of("ahmet","ali","mehmet","ayse","murat");
        s4.map(String::toUpperCase).forEach(System.out::println);

        Stream<String> s5=Stream.of("ahmet","ali","mehmet","ayse","murat");
        s5.map(String::length).forEach(System.out::print);

        System.out.println();
        double sum=DoubleStream.of(1.0,4.0,9.0).map(Math::sqrt).peek(System.out::println).sum();

        System.out.println(sum);

        //limit and skip
        Stream.iterate(10,n->n+2).limit(5).forEach(System.out::println);

        //skip ile ilk n elemanı atlıyoruz
        Stream.iterate(10,n->n+2).skip(2).limit(5).forEach(System.out::println);

        //flatMap ???

        //sorted
        Stream<String> s6=Stream.of("ahmet","ali","mehmet","ayse","murat");
        s6.sorted().forEach(System.out::println); //natural-order'a göre sıraladı

        Stream<String> s7=Stream.of("ahmet","ali","mehmet","ayse","murat");

        s7.sorted(Comparator.reverseOrder()).forEach(System.out::println);



        Stream.generate(()->"Infinite").filter(n->n.length()>4).sorted().limit(2).forEach(System.out::println);
        //sonsuz datayı sort etmeye çalışırsak out-of-memory hatası alırız.O yüzden fonksiyonların sırası önemli

        Stream.generate(()->"Infinite").filter(n->n.length()>4).limit(2).sorted().forEach(System.out::println);

    }
}
