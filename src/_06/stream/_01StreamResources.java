package _06.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class _01StreamResources {
    public static void main(String[] args) {
        //1den 10a kadar olan(10 hariç) sayılardan bir stream oluşturur.
        //Bu metot geriye yine bir IntStream döner.
        IntStream.range(1,10);

        //bu sefer 10 da dahil
        IntStream.rangeClosed(1,10);

        //bu da iteration yaparak bir stream oluşturuyor.
        IntStream.iterate(1,i->i+1);
        //Limit vermezsek sonsuza kadar gider.
        IntStream.iterate(1,i->i+1).limit(5);

        Stream<String> empty=Stream.empty();
        Stream<String> s1=Stream.of("data1","data2","data3","data4");

        List<String> list= Arrays.asList("a","b","c");
        Stream<String> s2=list.stream();

        IntStream intStream=Arrays.stream(new int[]{1,2,3});
        Stream stream=Arrays.stream(new Integer[]{1,2,3});


    }
}
