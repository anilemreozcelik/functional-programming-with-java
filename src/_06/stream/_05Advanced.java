package _06.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class _05Advanced {

    public static void main(String[] args) {
        List<String> list=new ArrayList<>();
        list.add("test");
        list.add("test2");
        Stream<String> stringStream=list.stream();
        list.add("test3");//buraya eklenene eleman stream'e eklenir mi?

        //Streamler lazy-evaluated olduğu için evet eklenir.
        System.out.println(stringStream.count());
    }
}
