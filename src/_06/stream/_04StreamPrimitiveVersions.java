package _06.stream;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class _04StreamPrimitiveVersions {
    public static void main(String[] args) {
        IntStream is=Stream.of(1,2,3,4,5).mapToInt(t->t);

        IntStream is1=Stream.of("1","2","3","4","5").mapToInt(t->Integer.parseInt(t));
        IntStream is2=Stream.of("1","2","3","4","5").mapToInt(Integer::parseInt);

    }
}
