package _06.stream;


import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class _03StreamTerminalOps {

    public static void main(String[] args) {

        Stream<String> s1=Stream.of("element1","element2","element3");
        long c=s1.count();

        //terminal ops aynı stream üzerinden 1 kere çağrılabilir.2.si hata alır
        //long c2=s1.count();

        Stream<String> s2=Stream.of("dog","horse","monkey");
        Optional<String> minLengthElement=s2.min((str1, str2)->str1.length()-str2.length());
        System.out.println(minLengthElement);

        Stream<String> s3=Stream.of("dog","horse","monkey");
        Optional<String> maxLengthElement=s3.max((str1,str2)->str1.length()-str2.length());
        System.out.println(maxLengthElement);

        IntStream is=IntStream.of(2,4,6,10,-1,100,5,200,-20);
        OptionalInt optionalInt=is.min();
        System.out.println(optionalInt);

        Stream<String> s4=Stream.of("element1","element2","element3");
        Optional<String> opt=s4.findFirst();
        System.out.println(opt);

        List<String> list= Arrays.asList("monkey","2","chimp");
        Stream<String> infinite=Stream.generate(()->"chimp");
        Predicate<String> pred= x->Character.isLetter(x.charAt(0));
        System.out.println(list.stream().anyMatch(pred));
        System.out.println(list.stream().allMatch(pred));
        System.out.println(list.stream().noneMatch(pred));

        System.out.println();
        //System.out.println(infinite.noneMatch(pred)); //daha ilk değerden false
        //System.out.println(infinite.anyMatch(pred));  //daha ilk değerden true
        //System.out.println(infinite.allMatch(pred)); //infinite olduğu için bu sonuç vermez


        Stream<String> s5=Stream.of("element1","element2","element3");
        s5.forEach(System.out::println);

        //reduce
        String[] array=new String[]{"w","o","l","f"};
        String result="";
        for (String s:array){
            result=result+s;
        }

        BinaryOperator<String> binaryOperator=(str1,str2)->str1+str2;
        Optional<String> opt1=Arrays.stream(array).reduce(binaryOperator);
        System.out.println(opt1.get());

        Optional<String> opt2=Arrays.stream(array).reduce(String::concat);
        System.out.println(opt2.get());

        //collect
        List<String> stringList=Arrays.stream(array).collect(Collectors.toList());
        System.out.println(stringList);


        Set<String> stringSet=Arrays.stream(array).collect(Collectors.toSet());
        System.out.println(stringSet);


        Stream<String> s6=Stream.of("dog","horse","monkey");
        Map<String,Integer>  stringIntegerMap=s6.collect(Collectors.toMap(str->str,str->str.length()));

        stringIntegerMap.forEach((name,len)-> System.out.printf("%s - %d \n",name,len));

    }
}
