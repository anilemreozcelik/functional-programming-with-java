package behaviorparameterization;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Test {

    public static void main(String[] args) {

        List<String> stringList= Arrays.asList("red","green","purple","yellow");
        List<Apple> appleList=Arrays.asList(new Apple(30.0,Color.RED),new Apple(20.0,Color.GREEN),new Apple(10,Color.RED));
        List stringresult=filter(stringList,(String s)->s.equals("green"));
        System.out.println(stringresult);
        List appleResult=filter(appleList,(Apple apple)->apple.getWeight()>=20);
        System.out.println(appleResult);


    }

    public static <T> List<T> filter(List<T> list, Predicate<T> p){
        List<T> result=new ArrayList();
        for (T t:list){
            if (p.test(t)){
                result.add(t);
            }
        }
        return result;
    }
}
