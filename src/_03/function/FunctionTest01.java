package _03.function;

import java.util.function.Function;
import java.util.stream.Stream;

public class FunctionTest01 {

    public static void main(String[] args) {
        Function<String,Integer> lengthFunc=str->str.length();
        Stream.of("hello","world","message").map(lengthFunc).forEach(System.out::println);

        Function<String,Integer> parseInt=Integer::parseInt;
        Stream.of("1","2","3").map(parseInt).forEach(System.out::println );
    }
}
