package immutable;

class ImmutabilityOfObject {
    private String name;

    /*public void setName(String name) {
        this.name = name;
    }*/

    public String getName() {
        return name;
    }


    //bir sınıftaki metodun davranışının alt sınıflarda değiştirilmesini istemiyorsam o metodu final yapmam gerekir.
    public final void displayMessage(){
        System.out.println("Hello "+name);
    }
}

//Bir sınıfı immtable yapmak istiyorsam o sınıfın memberlarını private yapmam gerekir ki
//değeri değiştirilemesin.Ayrıca setter da olmamalı
class AccessMembers{
    ImmutabilityOfObject ofObject=new ImmutabilityOfObject();

    public void access(){
        //ofObject.name="Anil";
        //ofObject.setName("Anil");
    }
}
public class ImmutableTest{
    public static void main(String[] args) {



    }
}
