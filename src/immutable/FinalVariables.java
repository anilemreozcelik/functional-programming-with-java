package immutable;

import java.util.Arrays;
import java.util.List;

public class FinalVariables {

    final int product_id;
    //bir değişkeni final tanımladıysak ya tanımlandığı yerde initialize etmemiz gerekir
    //yada constructor ile set etmemiz gerekir

    public FinalVariables(int product_id) {
        this.product_id=product_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    /*public void setProduct_id(int product_id){
        this.product_id=product_id;
        //setter tanımlaması yapamayız çünkü o değişkenin değerini değiştiremeyiz.
    }*/

    int name;
    final static int CATEGORY_ID;
    //static alanlar final tanımlandığında ya tanımlanıdğı yerde initialize edilmeli
    //yada bir static kod bloğu içinde initialize edilmeli.
    //Çünkü static alanlar nesneden bağımsız program ilk çalıştığında jvm'e yüklenir.
    //bu yüzden nesne oluşturulmamış olsa bile değerlerinin belli olması gerekir.
    //dolayısıyla constructor içerisinde init edemeyiz.
    static {
        CATEGORY_ID=15;
    }



    public static void main(String[] args) {
        FinalVariables test=new FinalVariables(4);
        //test.product_id=4;
        //test.product_id=3;

        final FinalVariables test1=new FinalVariables(5); //bu değişkeni final yapmamız onun fieldlarının final olması anlamına gelmez.
        test1.name=4;
        //test1=null; final olduğu için tekrar atama yapamam.

        final List<FinalVariables> testList= Arrays.asList(new FinalVariables(2));
        //burada testList referansının kendisi final yani o referansa yeniden
        //bir atama yapamam.Ama listenin içerisine eleman ekleyip çıkarabilirim,
        //elemanları set edebilirim.
        //testList=null; bunu YAPAMAM
        testList.add(new FinalVariables(3));
        testList.set(0,new FinalVariables(2));

        //Fakat List.of() ile oluşturduğumuz listeler unmodifiable list olduğundan
        //o listelerin elemanlarını değiştiremeyiz.
    }
}


