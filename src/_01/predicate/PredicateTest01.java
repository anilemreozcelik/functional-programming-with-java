package _01.predicate;

import java.util.function.Predicate;
import java.util.stream.Stream;

public class PredicateTest01 {

    public static void main(String[] args) {
        Stream.of("hello","world","sample","data","hi")
                .filter(str->str.startsWith("h")).forEach(System.out::println);

        //yada bu şekilde ayrı ayrı da yazabiliriz.
        Predicate<String> filterLetterH= str->str.startsWith("h");
        Stream.of("hello","world","sample","data","hi").filter(filterLetterH).forEach(System.out::println);

    }
}
