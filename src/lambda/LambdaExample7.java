package lambda;

import java.util.*;

public class LambdaExample7 {

    public static void main(String[] args) {
        List<String> stringList=new ArrayList<String>();
        stringList.add("ozcelik");
        stringList.add("emre");
        stringList.add("anil");
        stringList.forEach(str-> System.out.println(str));
        Comparator<String> compareByAlphabetical= (str1,str2)->str1.compareTo(str2);
        Collections.sort(stringList,compareByAlphabetical);
        stringList.forEach(str-> System.out.println(str));

    }
}
