package lambda;

interface Math{
    int calculate(int arg1,int arg2);
}

public class LambdaExample1 {

    public int doMath(Math math, int arg1, int arg2){
        return math.calculate(arg1,arg2);
    }

    public static void main(String[] args) {

        //Lambda ifadeleri tek bir soyut metoda sahip arayüzleri gerçekleştiren ifadelerdir.

        //with lambda expression
        LambdaExample1 example1=new LambdaExample1();
        Math adder=(int arg1,int arg2) -> {return arg1+arg2;};
        example1.doMath(adder,3,5);
        Math multiplier=(int arg1,int arg2)->{return arg1*arg2;};
        example1.doMath(multiplier,3,5);

        //with anonymous class
        Math divider=new Math() {
            @Override
            public int calculate(int arg1, int arg2) {
                return arg1/arg2;
            }
        };
        example1.doMath(divider,10,2);

        //with lambda exp(shorter version)
        Math mod=(a,b)->a%b;
        example1.doMath(mod,5,2);

    }
}
