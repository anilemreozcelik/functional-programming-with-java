package lambda;

@FunctionalInterface
interface ScopeInterface{
    int m=1; //public static final

    default String mymethod(){
       return "DefaultMethod";
    }

    public int f(int i,String s);
}


public class LambdaExample2 {
    private int i=3;
    private String s="Instance Variable";
    private static boolean b=false;

    public static void main(String[] args) {
        //burada tanımlanan değişkenleri eğer lambda expression içerisinde kullanacaksak
        //bunların final yada effectively final olarak tanımlanmış olması gerekir.
        int i=5;
        String s="In main()";
        //i++; i'nin değerini bu şekilde değiştirirsem artık effectively final değildir.Bu yüzden aşağıda lambda içinde kullanılmaz.
        //s="I love java";
        LambdaExample2 example2=new LambdaExample2();
        //instance değişşkenleri için final veya effectively final olma durumu yoktur.
        //Bunların değerini değiştirsek bile lambda içinde kullanabiliriz.
        example2.s="Test";
        example2.i++;

        ScopeInterface si=(ii,ss) -> {
            int m=2; //m değişkeni lambda expression tanımlanırken yukarıdaki interface'den devralınmaz.
                    // bu yüzden burada m değişkeni tanımlayabilirim.
            System.out.println(i); //i değişkenini final tanımlandığı sürece veya final tanımlanmasa bile değeri değişmediği sürece
            //burada kullanabilirim
            System.out.println(s);
            //example2=new LambdaExample2(); yukarıdaki example2 değişkeninin de final yada effectively final olması gerektiği için
            //bunu da yapamayız.

            //Fakat instance değişkenleri için bu durum geçerli değildir.Aşağıdaki gibi bu değişkenlerin değerleri ile oynayabiliriz.
            example2.i++;
            example2.s="A new value";
            System.out.println(example2.s);

            // cannot be referenced from a static context
            //this.i;


            return 1;
        };

        //lambda ifadelerinden gerçekleştirdiği arayüzün public,static,final olan değişkenlerine
        // ve default tanımlı metotlarına erişebiliriz
        System.out.println(si.m);
        System.out.println(si.mymethod());


    }
}
