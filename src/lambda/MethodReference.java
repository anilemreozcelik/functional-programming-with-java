package lambda;

import java.util.Date;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class MethodReference {
    public static void main(String[] args) {
        Consumer<String> print=(s1)-> System.out.println(s1);
        Consumer<String> printRef=System.out::println;
        printRef.accept("Hello Method Reference!");

        Consumer<Book> bookConsumer=book -> book.printInfo();
        bookConsumer.accept(new Book("test","me"));

        Consumer<Book> bookConsumer2=Book::printInfo;
        bookConsumer2.accept(new Book("test2","you"));

        Supplier<Date> dateSupplier=() -> new Date();
        System.out.println(dateSupplier.get());

        Supplier<Date> dateSupplier1=Date::new;
        System.out.println(dateSupplier1.get());
    }

}
