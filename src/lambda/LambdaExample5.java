package lambda;

import java.util.Comparator;
import java.util.Date;
import java.util.function.*;

public class LambdaExample5 {

    static BiFunction<Integer,Integer,Boolean> isGreaterThan=(num1,num2)->num1>num2;
    //aynısını predicate ile daha kolay yazabiliriz.
    static BiPredicate<Integer,Integer> isGreaterThanPredicate=(num1,num2)->num1>num2;
    static BiFunction<Integer,Integer,Integer> giveMax=(num1,num2)-> java.lang.Math.max(num1,num2);

    //Consumers
    static Consumer<String> lowerCasePrint=s-> System.out.println(s.toLowerCase());
    static BiConsumer<String,String> concatTwoString= (s,s2)-> System.out.println(s.concat(s2));

    //Suppliers
    static Supplier<String> getMessage=()->"Java is fun!";
    static Supplier<Date>  getDate=()->new Date();

    //Predicates
    static Predicate<String> stringLen=s->s.length()>10;
    static Predicate<Integer> isPositive=num->num>0;
    static Predicate<Integer> isEven=num->num%2==0;

    //Functionların özelliştirilmiş başka hallederi de var.
    // IntFunction,DoubleFunction,LongFunction,DoubleToLongFunction vs.
    static IntFunction<String> convertToString=(num)->"Num is:"+num;

    //BinaryOperator extends BiFunction(2 parametre alır,1 result döner)
    //BinaryOperator aynı tipten 2 parametre alır aynı tipten result döner
    static BinaryOperator<Integer> add=(a,b)->a+b;
    static BinaryOperator<Integer> max=(a,b)-> java.lang.Math.max(a,b);


    public static void main(String[] args) {
        //Function tek argüman alıp tek değer dönderir.
        //Function<T,R>
        //BiFunction ise 2 tane argüman alıp tek değer dönderir.
        //BiFunction<T,U,R>

        System.out.println(isGreaterThan.apply(10,5));
        System.out.println(isGreaterThanPredicate.test(5,10));
        System.out.println(giveMax.apply(4,5));

        lowerCasePrint.accept("My Name Is Anil");
        concatTwoString.accept("My","Name");

        System.out.println(getMessage.get());
        System.out.println(getDate.get());

        System.out.println(stringLen.test("myname"));
        System.out.println(isPositive.negate().test(-1));//elinizdeki predicate'in ters değerini dönderir

        //birden fazla predicate i and yada or ile birleştirebiliriz.
        System.out.println(isPositive.and(isEven).test(5));
        System.out.println(isPositive.and(isEven).test(6));
        System.out.println(isPositive.or(isEven).test(-2));

        //binaryop
        System.out.println(add.apply(2,3));
        System.out.println(max.apply(2,3));

    }
}
