package lambda;

import java.util.Scanner;
import java.util.function.Function;

public class LambdaExample6 {

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("Please enter an integer:");
        int i=scanner.nextInt();

        Function<Integer,Integer> twice=num->num*2;
        Function<Integer,Integer> square=num->num*num;

        int result=twice.apply(i);
        result=square.apply(result);
        System.out.println(result);

        Function<Integer,Integer> twiceAndSquare=twice.andThen(square);
        result=twiceAndSquare.apply(i);
        System.out.println(result);

        Function<Integer,Integer> squareAndTwice=square.andThen(twice);
        result=squareAndTwice.apply(i);
        System.out.println(result);





    }
}
