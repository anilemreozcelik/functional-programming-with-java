package lambda;

public class EffectivelyFinalTest {

    int number;


    //effectively final=>final olarak tanımlanmamış bir değişkene
    //sadece 1 kez atama yapıldıysa bu effectively final oluyor.
    //Sonradan değerinin değiştirilmemiş olması gerekiyor.
    public static void main(String[] args) {

        EffectivelyFinalTest obj=new EffectivelyFinalTest();
        //instance değişkenler için final yada effectively final olması gerekmiyor.
        //O kural local değişkenler için geçerli
        obj.number=20;
        obj.number=10;
        String localVar="Java";
        //localVar="compileError";
        // değerini değiştirirsem artık effectively final olmaz.
        //bu durumda lambda ifadesinde kullanamayız
        //lambda ifadesinde kullanmamız için ya final yada effectively final olmalı
        LambdaMessage lambda1=str-> System.out.println(str+" "+localVar+" "+obj.number);
        lambda1.printMessage("test");

    }
}

@FunctionalInterface
interface LambdaMessage{
    public void printMessage(String msg);
}
