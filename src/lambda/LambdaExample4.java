package lambda;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class LambdaExample4 {

    public static void main(String[] args) {
        int increment=incrementByOne(1);
        System.out.println(increment);

        int increment2=incrementByOneFunction.apply(1);
        System.out.println(increment2);

        int multiply=multiplyBy10Function.apply(increment2);
        System.out.println(multiply);

        int multiply2=addBy1AndThenMultiplyBy10.apply(1);
        System.out.println(multiply2);

        boolean result=isEven.apply(10);
        boolean resultWithPredicate=isEvenWithPredicate.test(11);

        List<String> list=List.of("test","lambda","myname","","a");
        Integer[] resultarray=getLengthOfTheStringsInList.apply(list);
        System.out.println(Arrays.asList(resultarray));
    }

    static Function<Integer,Integer> incrementByOneFunction=number->number+1;
    static Function<Integer,Integer> multiplyBy10Function=number->number*10;
    static Function<Integer,Integer> addBy1AndThenMultiplyBy10=incrementByOneFunction.andThen(multiplyBy10Function);

    static Function<Integer,String> converter= num->Integer.toString(num);
    static Function<Integer,Integer> square=num->num*num;
    static Function<Integer,Double> squared=num-> java.lang.Math.sqrt(num);

    static Function<Integer,Boolean> isEven=num->num%2==0;
    //bunu predicate ile daha rahat yazabiliriz

    static Predicate<Integer> isEvenWithPredicate=num->num%2==0;
    //predicate ile yazınca apply metodu değil test metodu ile çalıştırırız.

    //bir listedeki stringlerin uzunluğunu alıp bir diziye atan ve o diziyi dönen lambda expression
    static Function<List<String>,Integer[]> getLengthOfTheStringsInList=list->{
        Integer[] length=new Integer[list.size()];
        for (int i=0;i<list.size();i++){
            length[i]=list.get(i).length();
        }
        return length;
    };

    static Function<Integer[],Integer> arrayLengthMeasurer=array->array.length;
    private static int incrementByOne(int number) {
        return number+1;
    }

}
