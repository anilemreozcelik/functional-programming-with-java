package lambda;

@FunctionalInterface
interface Animal{
    public void eat();
}
@FunctionalInterface
interface Dog extends Animal{

}

@FunctionalInterface
interface Cat extends Animal{
    public void eat();  //yeni bir method değil override ediyor.O yüzden functional interface
}

//@FunctionalInterface
interface Bird extends Animal{
    public void fly(); //yeni bir abstract method geldi o yüzden functional değil
}

//@FunctionalInterface
interface Turtle{
    //hiç abstract method yok
}

//@FunctionalInterface
interface Lion{
    public boolean equals(Object obj);
    //object sınıfında yer alan methodlar sayılmıyor.O yüzden functional değil
}

public class FunctionalInterfaceTest {
}


