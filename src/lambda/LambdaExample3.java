package lambda;

public class LambdaExample3 {
    public static void main(String[] args) {
        LambdaExample3 lambdaExample3=new LambdaExample3();

    }

    public void start(){

    }
}

@FunctionalInterface
interface PropertyInterface{
    int aProperty=1;
    int doIt(int i,int j);
    public static void sMethod(){
        System.out.println("in sMethod()");
    }
    public default void dMethod(){
        System.out.println("in dMethod()");
    }
}

@FunctionalInterface
interface SubPropertyInterface extends PropertyInterface{
    //public int doThat(int i,int j);
    // Cant do that.Çünkü sadece 1 tane abstract method tanımlayabiliriz.O da yukarıda tanımlı
    //Ama yine abstract olmayan method tanımları yapabiliriz.
    int aSubProperty=3;
    public static void subSMethod(){
        System.out.println("int sMethod");
    }
    public default void subDMethod(){
        System.out.println("int dMethod");
    }
}
