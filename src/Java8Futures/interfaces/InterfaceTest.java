package Java8Futures.interfaces;

interface Fly{

    public abstract int getWingSpan() throws Exception;

    public default void eat(){
        System.out.println("Animal is eating");
    }

    public static double calculateSpeed(float distance,double time){
        return distance/time;
    }

}

interface SubFly extends Fly{
    public abstract int getWingSpan() throws Exception;

    @Override
    public default void eat(){
        System.out.println("SubFly is eating");
    }

    public static double calculateSpeed(float distance,double time){
        return 14;
    }

}

class Falcon implements SubFly{

    @Override
    public void eat() {
        System.out.println("Falcon is eating");
    }

    @Override
    public int getWingSpan() throws Exception {
        return 20;
    }
}

class Eagle implements Fly{

    @Override
    public int getWingSpan() throws Exception{
        return 15;
    }

    //default metotlar override edilebilir.
    //Fakat default keyword sınıflarda kullanılamaz bu yüzden onu kaldırıyoruz.
    @Override
    public void eat() {
        System.out.println("Eagle is eating");
    }
}

public class InterfaceTest {
    public static void main(String[] args) throws Exception {

        Fly flyEagle=new Eagle();
        System.out.println(flyEagle.getWingSpan());

        //interface üzerinde tanımlı static metotları referans üzerinden çağıramayız.
        //Interface ismi ile çağırmamız gerekir.
        //flyEagle.calculateSpeed(10,5);
        Fly.calculateSpeed(10,5);

        flyEagle.eat();
    }
}
