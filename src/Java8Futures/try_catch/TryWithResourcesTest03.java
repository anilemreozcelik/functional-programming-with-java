package Java8Futures.try_catch;

class Playstation implements AutoCloseable{
    int num;
    public Playstation(int num){
        this.num=num;
    }
    @Override
    public void close() throws RuntimeException {
        System.out.println("ShutDown: "+num);
    }
}
public class TryWithResourcesTest03 {

    public static void main(String[] args) {

        //Kaynaklar açıldığı sıranın tersinde kapatılırlar
        try(Playstation p1=new Playstation(1);
            Playstation p2=new Playstation(2)) {
            throw new RuntimeException();
        }catch (Exception e){
            System.out.println("Catch Block");
        }finally {
            System.out.println("Finally");
        }
    }
}
