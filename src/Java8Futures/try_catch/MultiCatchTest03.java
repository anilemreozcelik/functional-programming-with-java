package Java8Futures.try_catch;

import java.io.FileNotFoundException;
import java.io.IOException;

public class MultiCatchTest03 {
    public static void main(String[] args) {

        try {
            throw new IOException();
        }catch (IOException e){
            e=new FileNotFoundException();
        }

        //multicatch yapısında catch bloğunda yakalanan exceptiona tekrar
        //atama yapılmasına izin yok.Normal catch yapısında ise yukarıda olduğu gibi izin veriliyor.
        /*try {
            throw new IOException();
        }catch (IOException | RuntimeException e){
            e=new IOException();
        }*/
    }

    public static void test() throws FileNotFoundException,SecurityException{

    }
}
