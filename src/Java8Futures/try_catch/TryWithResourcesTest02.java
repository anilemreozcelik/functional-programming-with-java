package Java8Futures.try_catch;

import java.io.FileNotFoundException;

class Computer implements AutoCloseable{
    @Override
    public void close() throws RuntimeException {
        System.out.println("ShutDown!");
        throw new IllegalArgumentException("IllegalArgumentException");
    }
}
public class TryWithResourcesTest02 {

    public static void main(String[] args) {
        try(Computer c=new Computer()) {
            System.out.println("Try Block");
            throw new IndexOutOfBoundsException("IndexOutOfBoundsException");
        }catch (Exception e){
            System.out.println("Catch Block");
            System.out.println(e.getMessage());
            System.out.println();
            System.out.println("Suppressed Exceptions:");
            for (Throwable t:e.getSuppressed()){
                System.out.println(t.getMessage());
            }
        }
    }
}
