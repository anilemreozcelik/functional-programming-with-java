package Java8Futures.try_catch;

import java.io.FileNotFoundException;
import java.io.IOException;

public class MultiCatchTest04 {

    public static void main(String[] args) {
        try {
            //
        } catch (Exception e) {

        }

        //FileNotFoundException bir checked exception olduğundan
        //fırlatılmadığı sürece yakalanmasına izin verilmez
        //does not compile
        /*try {

        }catch (FileNotFoundException e){

        }*/

        try {
            //extends RuntimeException olduğundan fırlatmasa bile catch
            //bloğunda izin verilir.
        } catch (IllegalArgumentException e) {

        }


        try {
            test();
        } catch (FileNotFoundException | IllegalStateException e) {

        } catch (IOException e) {

        }
    }

    public static void test() throws IOException, SecurityException {
    }
}