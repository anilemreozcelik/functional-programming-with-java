package Java8Futures.try_catch;

import java.io.FileNotFoundException;

public class MultiCatchTest01 {
    public static void main(String[] args) {

        try {
            test();
        }catch (FileNotFoundException | SecurityException e){
            e.printStackTrace();
        }


        //DOES NOT COMPILE
        /*try {
            test();
        }catch (FileNotFoundException f | SecurityException s){
            e.printStackTrace();
        }*/

        /*try {
            test();
        }catch (FileNotFoundException e | SecurityException e){
            e.printStackTrace();
        }*/




    }

    public static void test() throws FileNotFoundException,SecurityException{

    }
}
