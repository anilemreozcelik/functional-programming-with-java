package Java8Futures.try_catch;

import java.io.FileNotFoundException;
import java.io.IOException;

public class MultiCatchTest02 {

    public static void main(String[] args) {

        //DOES NOT COMPILE.FileNotFoundException is subclass of IOException
        //Multicatch yapısıyla kullanamayız.Fakat ayrı catchlerde kullanabiliriz.
        /*try {
            test();
        }catch (FileNotFoundException | IOException e){

        }*/

        /*try {
            test();
        }catch ( IOException | FileNotFoundException e){

        }*/

        /*try {
            test();
        }catch ( IOException | IOException e){

        }*/

        //legal
        try {
            test();
        }catch (FileNotFoundException e){

        }catch (IOException e){

        }
    }

    public static void test() throws FileNotFoundException,SecurityException{

    }
}
