package Java8Futures.try_catch;

import java.io.*;
import java.nio.file.Files;
import java.util.Scanner;

public class TryWithResourcesTest01 {
    public static void main(String[] args) {
        //bu yeni yaklaşımda try bloğunda açılan bütün resourcelar
        //otomatik olarak kapatılır.Eski yaklaşımdaki gibi
        //finally bloğunda kendimizin kapatmasına gerek kalmaz.

        //Old Way
        FileWriter fileWriter=null;
        try {
            fileWriter=new FileWriter("file.txt");
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try {
                fileWriter.close();
            }catch (IOException e){

            }
        }

        //New Way
        //Closable Interface'ini implement eden sınıflar bu bloğa yazılabilir.
        try(FileWriter w=new FileWriter("file.txt")) {
            w.write("test");
        }catch (IOException e){

        }

        try(Scanner s=new Scanner(System.in)){
            s.nextLine();
        }catch (Exception e){
            //s.nextLine(); Out Of Scope
        }
        //s.close() metodunu çağırmaya gerek yok.



    }
}
