package Java8Futures.formatter;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;

public class DateTimeFormatterTest01 {
    public static void main(String[] args) {
        DateTimeFormatter customFormat=DateTimeFormatter.ofPattern("dd MM yyyy");
        System.out.println(customFormat.format(LocalDate.of(2016, Month.AUGUST,01)));

        DateTimeFormatter customFormatter02=DateTimeFormatter.ofPattern("HH:mm");
        LocalTime time=LocalTime.of(20,27);
        System.out.println(customFormatter02.format(time));
    }
}
