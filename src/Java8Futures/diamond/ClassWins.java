package Java8Futures.diamond;
class BaseClass{
    public void foo(){
        System.out.println("BaseClass#foo");
    }
    public void goo(){
        System.out.println("BaseClass#goo");
    }
}

interface BaseInterface{
    default public void foo(){
        System.out.println("BaseInterface#foo");
    }

    public abstract void goo();
}


public class ClassWins extends BaseClass implements BaseInterface {

    /*@Override
    public void goo() {
        System.out.println("ClassWins#goo");
    }*/

    //sınıf ile interface metotları çakıştığında sınıf metotları kazanır.
    public static void main(String[] args) {
        new ClassWins().foo();

        new ClassWins().goo();
    }
}
