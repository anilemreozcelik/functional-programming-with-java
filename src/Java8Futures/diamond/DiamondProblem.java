package Java8Futures.diamond;


//interface'ler birden fazla interface'i kalıtabiliyor.Bu yüzden diamond problem ortaya çıkıyor.
public class DiamondProblem implements Interface1,Interface2{


    //bunu çözmek için o metodu override etmek gerek
    @Override
    public void defaultTestMethod(){
        Interface1.super.defaultTestMethod();
    }

}


interface Interface1{

    public default void defaultTestMethod(){
        System.out.println("Interface1#defaultTestMethod");
    }
}


interface Interface2{
    public default void defaultTestMethod(){
        System.out.println("Interface1#defaultTestMethod");
    }
}