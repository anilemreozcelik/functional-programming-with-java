package Java8Futures.duration;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;

public class DurationTest01 {
    public static void main(String[] args) {
        LocalTime localTime=LocalTime.of(10,30);
        LocalTime now=LocalTime.now();

        Duration duration=Duration.between(localTime,now);
        System.out.println(duration); //PT5H28M27.3784617S

        Duration d1=Duration.ofDays(1);
        Duration d2=Duration.ofHours(3);
        Duration d3=Duration.ofMinutes(20);
        Duration d4=Duration.ofSeconds(75);

        System.out.println(d1);
        System.out.println(d2);
        System.out.println(d3);
        System.out.println(d4);

        LocalDate start=LocalDate.of(2016,10,5);
        LocalDate end=LocalDate.now();

        //Duration,LocalDate ile kullanılamaz,patlar.
        Duration d5=Duration.between(start,end); //UnsupportedTemporalTypeException: Unsupported unit: Seconds
        System.out.println(d5);


        //Duration-->Time
        //Period-->Date
    }
}
