package Java8Futures.zone;

import java.time.ZoneId;
import java.util.Set;

public class ZoneTest {

    public static void main(String[] args) {
        ZoneId defaultZone=ZoneId.systemDefault();
        System.out.println(defaultZone);

        Set<String> avaibles=ZoneId.getAvailableZoneIds();
        avaibles.forEach((String s)-> System.out.println(s));
    }
}
