package Java8Futures.zone;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ZoneTest02 {

    public static void main(String[] args) {
        LocalDateTime localDateTime=LocalDateTime.now();
        ZonedDateTime zonedDateTime=ZonedDateTime.now();

        System.out.println(localDateTime);
        System.out.println(zonedDateTime);


        //ZonedDateTime=LocalDateTime+ZoneId
        ZoneId zoneId=ZoneId.of("Europe/Paris");
        ZonedDateTime zonedDateTime1=ZonedDateTime.of(localDateTime,zoneId);
        System.out.println(zonedDateTime1);

        LocalDate date=LocalDate.now(ZoneId.of("Asia/Singapore"));
        System.out.println(date);

    }
}
