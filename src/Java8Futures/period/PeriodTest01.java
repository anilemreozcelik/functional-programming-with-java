package Java8Futures.period;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;

public class PeriodTest01 {
    public static void main(String[] args) {
        LocalDate startDate=LocalDate.of(2014, Month.AUGUST,20);
        LocalDate endDate=LocalDate.now();

        Period period=Period.between(startDate,endDate);
        System.out.println(period); //P7Y5M22D

        Period period1=Period.ofYears(2);
        Period period2=Period.ofMonths(3);
        Period period3=Period.ofDays(22);
        Period period4=Period.ofWeeks(4);


        //yanlış!!
        Period period5=Period.ofDays(29).ofYears(1).ofMonths(2);

        System.out.println(period1);
        System.out.println(period2);
        System.out.println(period3);
        System.out.println(period4);
        System.out.println(period5);

        LocalDate date=LocalDate.now();
        System.out.println(date.plus(Period.ofMonths(2)));

        //LocalTime,Period ile çalışmaz.
        LocalTime time=LocalTime.now();
        System.out.println(time.plus(Period.ofMonths(2)));  //UnsupportedTemporalTypeException: Unsupported unit: Months
    }
}
