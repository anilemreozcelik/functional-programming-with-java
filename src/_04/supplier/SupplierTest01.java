package _04.supplier;

import java.util.Random;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class SupplierTest01 {
    public static void main(String[] args) {

        Supplier<Integer> supplier=()->new Random().nextInt(10);
        Stream.generate(supplier).limit(5).forEach(System.out::println);

        Supplier<String> supplier2=String::new;
        //Supplier<Integer> supplier3=Integer::new;  parametresiz constructor olmadığı için supplier ile kullanılamaz
        Function<String,Integer> function=Integer::new;

    }
}
